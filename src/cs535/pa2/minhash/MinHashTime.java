package cs535.pa2.minhash;

public class MinHashTime {

	private static long start;

	@SuppressWarnings("unused")
	public void timer(String folder, int numPermutations) {
		MinHash minHash = new MinHash(folder, numPermutations);
		String[] docs = minHash.allDocs();
		// Test 1-- Exact similarity among all pairs.
		start();
		for (int i = 0; i < docs.length; i++) {
			for (int j = i + 1; j < docs.length; j++) {
				minHash.exactJaccard(docs[i], docs[j]);
			}
		}
		System.out.println("Time taken for computing exact similarity for all pairs is : " + elapsedTime());

		// Test 2.a -- Compute MinHash Matrix
		start();
		int[][] minHashMatrix = minHash.minHashMatrix();
		System.out.println("Time taken for computing MinHash Matrix is : " + elapsedTime());

		// Test 2.b -- Compute similarities on all pairs
		start();
		for (int i = 0; i < docs.length; i++) {
			for (int j = i + 1; j < docs.length; j++) {
				int l = 0;
				for (int k = 0; k < numPermutations; k++) {
					if (minHashMatrix[k][i] == minHashMatrix[k][j])
						l++;
				}
				double sim = (double) l / numPermutations;
			}
		}
		System.out.println("Time taken for computing similarities using min hash matrix is : " + elapsedTime());

	}

	private void start() {
		start = System.currentTimeMillis();
	}

	private double elapsedTime() {
		return (System.currentTimeMillis() - start) / 1000.0;
	}


}
