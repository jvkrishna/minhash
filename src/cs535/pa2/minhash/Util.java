package cs535.pa2.minhash;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Utility class.
 * 
 * @author Vamsi Krishna J
 * @author Revanth A
 *
 */
public class Util {

	private static final String PUNCTUATION_REGEX = "[.,:;']";

	private static Predicate<? super Path> IGNORE_HIDDEN_FILES = path -> !path.toFile().isHidden();

	public static String[] getAllFileNames(Path folderPath) {
		try (Stream<Path> fileStream = Files.list(folderPath)) {
			return fileStream.filter(IGNORE_HIDDEN_FILES).map(Path::getFileName).map(Path::toString)
					.toArray(String[]::new);
		} catch (IOException e) {
			e.printStackTrace();
			return new String[] {};
		}

	}

	public static Set<String> getAllTermsFromDocumentCollection(Path folderPath) throws IOException {
		Set<String> allTerms = new HashSet<>();
		Files.newDirectoryStream(folderPath, path -> !path.toFile().isHidden()).forEach(filePath -> {
			Set<String> fileTerms = getAllTermsFromADocument(filePath);
			allTerms.addAll(fileTerms);
		});
		return allTerms;
	}

	public static Set<String> getAllTermsFromADocument(Path filePath) {
		Set<String> documentTerms = new HashSet<>();
		try (Stream<String> stream = Files.lines(filePath, StandardCharsets.ISO_8859_1)) {
			stream.filter(s -> !s.isEmpty()).map(String::toLowerCase).forEach(line -> {
				String[] words = line.replaceAll(PUNCTUATION_REGEX, "").split("\\s+");
				for (String word : words) {
					if (!(word.length() < 3 || word.equals("the") || documentTerms.contains(word))) {
						documentTerms.add(word);
					}
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
		return documentTerms;
	}

	public static Map<String, Set<String>> prepareDocumentMap(Path folderPath) {
		Map<String, Set<String>> documentsMap = new HashMap<>();
		for (String fileName : getAllFileNames(folderPath)) {
			Set<String> docTerms = getAllTermsFromADocument(folderPath.resolve(fileName));
			documentsMap.put(fileName, docTerms);
		}
		return documentsMap;
	}

	public static Map<String, Integer> extractAllTerms(Map<String, Set<String>> documents) {
		Map<String, Integer> termsMap = new HashMap<>();
		documents.values().stream().flatMap(terms -> terms.stream()).forEach(term -> {
			if (!termsMap.containsKey(term))
				termsMap.put(term, termsMap.size() + 1);
		});
		return termsMap;
	}

	public static int[][] generateKRandomPermuatations(int k, int m) {
		int[][] permutations = new int[k][m];
		for (int i = 0; i < k; i++) {
			permutations[i] = generateRandomPermutation(m);
		}
		return permutations;
	}

	public static int[] generateRandomPermutation(int m) {
		int[] arr = new int[m];
		for (int i = 0; i < m; i++) {
			arr[i] = i + 1;
		}
		ThreadLocalRandom ran = ThreadLocalRandom.current();
		for (int i = arr.length - 1; i > 0; i--) {
			int index = ran.nextInt(i + 1);
			int tmp = arr[index];
			arr[index] = arr[i];
			arr[i] = tmp;
		}
		return arr;
	}

	public static double jaccardSimilarity(Set<String> s1, Set<String> s2) {
		long intersection = s1.stream().filter(s -> s2.contains(s)).count();
		long union = s1.size() + s2.size() - intersection;
		return (double) intersection / union;

	}

	public static HashTable[] createBHashTables(Map<String, int[]> minHashMatrix, int bands,
			HashFunction hashFunction) {
		HashTable[] hashTables = new HashTable[bands];
		for (int i = 0; i < bands; i++) {
			hashTables[i] = new HashTable(hashFunction.getTableSize());
		}
		for (Entry<String, int[]> entrySet : minHashMatrix.entrySet()) {
			int[] minHashSig = entrySet.getValue();
			int r = minHashSig.length / bands;
			for (int i = 0; i < bands; i++) {
				int[] subArray = Arrays.copyOfRange(minHashSig, i * r, (i + 1) * r);
				int hash = hashFunction.hashTuple(subArray);
				hashTables[i].add(hash, entrySet.getKey());
			}
		}
		return hashTables;
	}

	public static int getNextPrime(int start) {
		// We are guaranteed to get a prime between start and 2*start
		if (start % 2 == 0)
			start++;
		for (int i = start; i < 2 * start; i = i + 2) {
			if (isPrime(i))
				return i;
		}
		throw new IllegalArgumentException("Unable to find the next prime number.");
	}

	public static boolean isPrime(int num) {
		if (num % 2 == 0 || num % 3 == 0)
			return false;
		int i = 5;
		while (i * i < num) {
			if (num % i == 0 || num % (i + 2) == 0)
				return false;
			i += 6;
		}
		return true;
	}

	public static void serialize(Object data, Path path) {
		try (FileOutputStream fos = new FileOutputStream(path.toString());
				ObjectOutputStream oos = new ObjectOutputStream(fos);) {
			oos.writeObject(data);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static Object deserialize(Path path) {
		try (FileInputStream fis = new FileInputStream(path.toString());
				ObjectInputStream ois = new ObjectInputStream(fis);) {
			return ois.readObject();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

}
