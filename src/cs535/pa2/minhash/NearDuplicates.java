package cs535.pa2.minhash;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Vamsi Krishna J
 * @author Revanth A
 *
 */
public class NearDuplicates {

	public List<String> nearDuplciateDetector(String folder, int numPermutations, double s, String docName) {

		MinHash minHash = new MinHash(folder, numPermutations);
		// int[][] minHashMatrix = minHash.minHashMatrix();
		// Util.serialize(minHashMatrix, Paths.get("minhash-600.dat"));
		int[][] minHashMatrix = (int[][]) Util.deserialize(Paths.get("minhash-600.dat"));
		int numberOfBands = solveForNumberOfBands(numPermutations, s);
		LSH lsh = new LSH(minHashMatrix, minHash.allDocs(), numberOfBands);
		ArrayList<String> nearDuplicates = lsh.nearDuplicatesOf(docName);
		nearDuplicates.forEach(System.out::println);
		return nearDuplicates;
	}

	/**
	 * Using the Newton-Raphson method, it solves for the number of bands. We know
	 * the, number of permutations (k) and the similarity (s) <br />
	 * (1/b)^(1/r) =s and rb =k <br/>
	 * b^b = 1/s^k => b*logb = k*log(1/s) <br />
	 * Reference :
	 * {@link https://math.stackexchange.com/questions/50316/xx-y-how-to-solve-for-x}
	 * 
	 * @param k
	 * @param similarity
	 * @return
	 */
	private int solveForNumberOfBands(int k, double s) {
		double d = k * Math.log(1 / s);
		double convergePoint = 0.01;
		// Initialize X0 with 1.
		double xn = 1;
		double next = nextApproximation(xn, d);
		while (Math.abs(next - xn) > convergePoint) {
			xn = next;
			next = nextApproximation(xn, d);
		}
		return (int) next;
	}

	private double nextApproximation(double xn, double d) {
		return xn - (((xn * Math.log(xn)) - d) / (1 + Math.log(xn)));
	}


}
