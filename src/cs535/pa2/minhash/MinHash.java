package cs535.pa2.minhash;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

/**
 * 
 * @author Vamsi Krishna J
 * @author Revanth A
 *
 */
public class MinHash {

	private Path folderPath;
	private int numPermutations;
	private Map<String, Set<String>> documentsMap;
	private Map<String, Integer> termsMap;
	private int[][] permutations;
	private Map<String, int[]> minHashSigMemo = new HashMap<>();

	/**
	 * In the constructor, we are building the documents map, terms map and the
	 * permutations.
	 * 
	 * @param folder
	 * @param numPermutations
	 */
	public MinHash(String folder, int numPermutations) {
		this.folderPath = Paths.get(folder);
		this.numPermutations = numPermutations;
		this.documentsMap = Util.prepareDocumentMap(Paths.get(folder));
		this.termsMap = Util.extractAllTerms(this.documentsMap);
		this.permutations = Util.generateKRandomPermuatations(numPermutations, this.termsMap.size());
	}

	public String[] allDocs() {
		return Util.getAllFileNames(this.folderPath);
	}

	public double exactJaccard(String file1, String file2) {
		Set<String> doc1 = documentsMap.get(file1);
		Set<String> doc2 = documentsMap.get(file2);
		if (doc1 == null || doc2 == null) {
			throw new RuntimeException("Unable to extract the document terms.");
		}
		return Util.jaccardSimilarity(doc1, doc2);
	}

	public int[] minHashSig(String fileName) {
		Set<String> docTerms = documentsMap.get(fileName);
		if (docTerms == null) {
			throw new RuntimeException("Unable to extract document");
		}
		int[] minHashSig = new int[this.numPermutations];
		for (int i = 0; i < this.numPermutations; i++) {
			Vector<Integer> termsVector = new Vector<>(docTerms.size());
			int[] permutation = permutations[i];
			for (String s : docTerms) {
				termsVector.add(permutation[termsMap.get(s) - 1]);
			}
			minHashSig[i] = Collections.min(termsVector);
		}
		return minHashSig;
	}

	public double approximateJaccard(String file1, String file2) {
		if (!minHashSigMemo.containsKey(file1)) {
			minHashSigMemo.put(file1, minHashSig(file1));
		}
		if (!minHashSigMemo.containsKey(file2)) {
			minHashSigMemo.put(file2, minHashSig(file2));
		}
		int[] minHashSig1 = minHashSigMemo.get(file1);
		int[] minHashSig2 = minHashSigMemo.get(file2);
		int l = 0;
		for (int i = 0; i < numPermutations; i++) {
			if (minHashSig1[i] == minHashSig2[i])
				l++;
		}
		return (double) l / this.numPermutations;
	}

	public int[][] minHashMatrix() {
		String[] docs = allDocs();
		int[][] minHashMatrix = new int[this.numPermutations][docs.length];
		for (int i = 0; i < docs.length; i++) {
			if (!minHashSigMemo.containsKey(docs[i])) {
				minHashSigMemo.put(docs[i], minHashSig(docs[i]));
			}
			int[] minHashSig = minHashSigMemo.get(docs[i]);
			for (int j = 0; j < minHashSig.length; j++) {
				minHashMatrix[j][i] = minHashSig[j];
			}
		}
		return minHashMatrix;
	}

	public int numTerms() {
		return termsMap != null ? termsMap.size() : 0;
	}

	public int numPermutations() {
		return this.numPermutations;
	}

}
