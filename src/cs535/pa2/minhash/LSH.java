package cs535.pa2.minhash;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 
 * @author Vamsi Krishna J
 * @author Revanth A
 *
 */
public class LSH {

	private Map<String, int[]> minHashMatrixMap;
	private int bands;
	private int numOfPermutations;
	private HashFunction hashFunction;
	private HashTable[] hashTables;

	public LSH(int[][] minHashMatrix, String[] docNames, int bands) {
		initialize(minHashMatrix, docNames, bands);
		this.hashTables = Util.createBHashTables(this.minHashMatrixMap, this.bands, this.hashFunction);
	}

	public ArrayList<String> nearDuplicatesOf(String docName) {
		if (!minHashMatrixMap.containsKey(docName)) {
			throw new IllegalArgumentException("Document min hash doesn't exist");
		}
		int[] minHashSig = minHashMatrixMap.get(docName);
		int r = minHashSig.length / bands;
		Set<String> similarDocs = new HashSet<>();
		for (int i = 0; i < this.bands; i++) {
			int[] rTuple = Arrays.copyOfRange(minHashSig, i * r, (i + 1) * r);
			int hash = this.hashFunction.hashTuple(rTuple);
			similarDocs.addAll(hashTables[i].get(hash));
		}
		List<String> similarDocsList = similarDocs.stream().filter(s -> !s.equals(docName))
				.collect(Collectors.toList());
		return new ArrayList<>(similarDocsList);
	}

	private void initialize(int[][] minHashMatrix, String[] docNames, int bands) {
		this.minHashMatrixMap = new HashMap<>();
		this.numOfPermutations = minHashMatrix.length;
		for (int i = 0; i < docNames.length; i++) {
			int[] minHashSig = new int[numOfPermutations];
			for (int j = 0; j < minHashSig.length; j++) {
				minHashSig[j] = minHashMatrix[j][i];
			}
			minHashMatrixMap.put(docNames[i], minHashSig);
		}
		this.bands = bands;
		this.hashFunction = new HashFunction(numOfPermutations / bands, Util.getNextPrime(docNames.length));
	}

}
