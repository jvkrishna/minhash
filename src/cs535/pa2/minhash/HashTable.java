package cs535.pa2.minhash;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Vamsi Krishna J
 * @author Revanth A
 *
 */
public class HashTable {
	private List<String>[] table;

	@SuppressWarnings("unchecked")
	public HashTable(int tableSize) {
		this.table = new ArrayList[tableSize];
		for (int i = 0; i < tableSize; i++) {
			this.table[i] = new ArrayList<>();
		}
	}

	public void add(int index, String val) {
		if (index >= table.length) {
			throw new IllegalArgumentException("Index out of bounds");
		}
		this.table[index].add(val);
	}

	public List<String> get(int index) {
		if (index >= table.length) {
			throw new IllegalArgumentException("Index out of bounds");
		}
		return table[index];
	}
}