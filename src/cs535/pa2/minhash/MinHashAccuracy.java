package cs535.pa2.minhash;

/**
 * 
 * @author Vamsi Krishna J
 * @author Revanth A
 *
 */
public class MinHashAccuracy {

	public int accuracy(String folder, int numPermutations, double error) {
		MinHash minHash = new MinHash(folder, numPermutations);
		String[] docs = minHash.allDocs();
		int errorPairs = 0;
		for (int i = 0; i < docs.length; i++) {
			for (int j = i + 1; j < docs.length; j++) {
				double exactSim = minHash.exactJaccard(docs[i], docs[j]);
				double approxSim = minHash.approximateJaccard(docs[i], docs[j]);
				if (Math.abs(exactSim - approxSim) > error)
					errorPairs++;
			}
		}
		System.out.printf("e=%f \t k =%d | Error pairs=%d \n", error, numPermutations, errorPairs);
		return errorPairs;
	}

}
