package cs535.pa2.minhash;

import java.util.concurrent.ThreadLocalRandom;

/**
 * @author Vamsi Krishna J
 * @author Revanth A
 *
 */
public class HashFunction {

	private int[] aArr;
	private int tableSize;

	public HashFunction(int r, int tableSize) {
		this.tableSize = tableSize;
		this.aArr = new int[r];
		ThreadLocalRandom random = ThreadLocalRandom.current();
		for (int i = 0; i < r; i++) {
			aArr[i] = random.nextInt(tableSize);
		}
	}

	public int hashTuple(int[] tuple) {
		long sum = 0;
		for (int i = 0; i < aArr.length; i++) {
			sum += aArr[i] * tuple[i];
		}
		return (int) (sum % tableSize);
	}
	
	public int getTableSize() {
		return tableSize;
	}

}